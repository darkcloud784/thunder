/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.warheadgaming.thunder;


import java.util.logging.Level;
import java.util.logging.Logger;
import org.spout.api.exception.ConfigurationException;
import org.spout.api.util.config.Configuration;
import org.spout.api.util.config.ConfigurationHolder;
import org.spout.api.util.config.ConfigurationHolderConfiguration;

/**
 *
 * @author DarkCloud
 */
public class ThunderConfig extends ConfigurationHolderConfiguration{
    
    private static Configuration config;
    private static ConfigurationHolder damage = new ConfigurationHolder(config,(Object) 5 , "damage amount");
    
    public ThunderConfig(Configuration config){
        super(config);
        this.config = config;
        load();
    }
    
    public static int getDamage(){
    return damage.getInt();
    }
    
    public void load(){
        try {
            super.load();
            super.save();
        } catch (ConfigurationException ex) {
            Logger.getLogger(ThunderConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void save(){
        try {
            super.save();
        } catch (ConfigurationException ex) {
            Logger.getLogger(ThunderConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
