/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.warheadgaming.thunder;

import com.warhead.warriors.api.SkillBindManager;
import com.warhead.warriors.api.trigger.PlayerLeftClickTrigger;
import com.warhead.warriors.api.trigger.TriggerHandler;
import com.warhead.warriors.api.trigger.TriggerListener;
import java.util.List;
import org.spout.api.Spout;
import org.spout.api.entity.Entity;
import org.spout.api.entity.Player;
import org.spout.api.geo.LoadOption;
import org.spout.api.geo.World;
import org.spout.api.geo.discrete.Point;
import org.spout.vanilla.component.entity.living.Living;
import org.spout.vanilla.component.entity.misc.Health;
import org.spout.vanilla.component.entity.substance.Lightning;
import org.spout.vanilla.data.effect.store.GeneralEffects;
import org.spout.vanilla.event.cause.EntityDamageCause;

/**
 *
 * @author DarkCloud
 */
public class ThunderListener extends TriggerListener {
 
    
    @TriggerHandler
    public boolean LeftClickTrigger(PlayerLeftClickTrigger leftClick) {
        Spout.getLogger().info("Thunder called");
        if (SkillBindManager.get().isBound(leftClick.getPlayer(), "Thunder")) {
            Spout.getLogger().info("thunder check passed");
            Player player = leftClick.getPlayer();
            World world = player.getWorld();
            Point point;
            double dist;
            List<Entity> victims = player.getWorld().getNearbyEntities(player, 5);
            for (Entity victim : victims) {
                point = victim.getScene().getPosition();
                if (victim.get(Living.class) != null && !victim.equals(player)) {
                    Player player2 = (Player) victim;
                    dist = victim.getScene().getPosition().distanceSquared(point);
                    float volume = (float) (10000F - Math.pow((dist), 0.73));
                    GeneralEffects.LIGHTNING_THUNDER.adjust(volume, 0.7F).play(player2, point);
                    victim.get(Health.class).damage(ThunderConfig.getDamage(), new EntityDamageCause(player, EntityDamageCause.DamageType.ATTACK));
                }
                world.createAndSpawnEntity(point, LoadOption.NO_LOAD, Lightning.class);
            }
            return true;
        }
        return false;
    }
    
    
}
