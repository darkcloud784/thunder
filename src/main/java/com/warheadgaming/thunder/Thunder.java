/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.warheadgaming.thunder;

import com.warhead.warriors.api.WarriorsManager;
import com.warhead.warriors.api.skill.Skill;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.spout.api.exception.ConfigurationException;
import org.spout.api.util.config.Configuration;
import org.spout.api.util.config.yaml.YamlConfiguration;

/**
 *
 * @author DarkCloud
 */
public class Thunder extends Skill {

    ThunderListener listener = new ThunderListener();
    Configuration config;

    @Override
    public void onEnable() {
        WarriorsManager.registerTriggerListener(this, listener);
        File file = new File("plugins/Warriors/Skills/Thunder/");
        if (!file.exists()) {
            file.mkdirs();
        }
        config = new ThunderConfig((Configuration) new YamlConfiguration(new File(file, "Thunder.yml")));
        try {
            config.load();
            config.save();
        } catch (ConfigurationException ex) {
            Logger.getLogger(Thunder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    

    @Override
    public void onDisable() {
    }
    
    
}
